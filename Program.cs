﻿﻿using System;

namespace EX_ThuThapQuang
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.Write("Nhập số mảnh quặng đã thu thập: ");
            int soMangQuang = int.Parse(Console.ReadLine());

            int xuDongVang = TinhXuDongVang(soMangQuang);

            Console.WriteLine("Số lượng đồng xu vàng mà người chơi nhận được là: " + xuDongVang);
        }

        public static int TinhXuDongVang(int mangQuang)
        {
            int xuDongVang = 0;

            if (mangQuang <= 10)
            {
                xuDongVang = mangQuang * 10;
            }
            else if (mangQuang <= 15)
            {
                xuDongVang = 10 * 10 + (mangQuang - 10) * 5;
            }
            else if (mangQuang <= 18)
            {
                xuDongVang = 10 * 10 + 5 * 5 + (mangQuang - 15) * 2;
            }
            else
            {
                xuDongVang = 10 * 10 + 5 * 5 + 3 * 2 + (mangQuang - 18);
            }

            return xuDongVang;
        }
    }
    
}